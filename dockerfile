FROM debian:jessie
MAINTAINER Mathieu Bossaert <sig@cenlr.org>

RUN echo "#" >> /etc/apt/sources.list \
    && echo "#Ajout pour GeoNature-atlas" >> /etc/apt/sources.list \
    && echo "deb http://httpredir.debian.org/debian jessie main" >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y apt-utils \ 
	&& apt-get install -y unzip \
	&& apt-get install -y wget \
	&& apt-get install -y apache2 \
	&& apt-get install -y libapache2-mod-wsgi \
	&& apachectl restart
RUN apt-get install -y postgresql postgis
RUN apt-get install -y python-setuptools \
	&& apt-get install -y libpq-dev python-dev \
	&& apt-get install -y python python-pip \
	&& apt-get install -y python-gdal \
	&& apt-get install -y gdal-bin
RUN apt-get install -y python-virtualenv
RUN adduser --disabled-password lambda
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN cd /home/lambda
RUN wget https://github.com/PnEcrins/GeoNature-atlas/archive/1.2.2.zip \
	&& unzip 1.2.2.zip
RUN mv GeoNature-atlas-1.2.2 /home/lambda/atlas \
	&& cd /home/lambda/atlas/
RUN mkdir /home/lambda/atlas/venv \
	&& chmod -R 777 /home/lambda/atlas
RUN chown lambda /home/lambda/atlas/venv
USER lambda
RUN cd /home/lambda/atlas/ \
	&& whoami \
	&& pwd \
	&& ./install_app.sh
USER root
